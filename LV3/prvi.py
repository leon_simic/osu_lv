import pandas as pd
import numpy as np

data = pd.read_csv("data_C02_emission.csv")

#a)
print('Broj mjerenja: ', len(data))

print(data.info())

print('Broj duplikata: ', len(data.duplicated()))
data.drop_duplicates()

data = data.astype({"Make":'category', "Model":'category', "Vehicle Class":'category', "Transmission":'category', "Fuel Type":'category'})
print(data.info())

#b)
najveca_potrosnja = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=False).head(3)
print('Najveca potrosnja u gradu: ', najveca_potrosnja[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

najmanja_potrosnja = data.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=True).head(3)
print('Najmanja potrosnja u gradu: ', najmanja_potrosnja[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#c)
vozila_motor_25_35 = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print('Broj vozila koji imaju motor veci od 2.5 i manji od 3.5 L: ', len(vozila_motor_25_35))
print('Njihova prosjecna emisija CO2: ', vozila_motor_25_35[['CO2 Emissions (g/km)']].mean())

#d)
print('Broj vozila marke Audi: ', len(data[data['Make'] == 'Audi']))

audi_4_cilindra = data[(data['Make'] == 'Audi') & (data['Cylinders'] == 4)]
print('Prosjecna emisija CO2 Audi s 4 cilindra: ', audi_4_cilindra[['CO2 Emissions (g/km)']].mean())

#e)
vozila_4_cilindra = data[data['Cylinders'] == 4]
print('Broj vozila sa 4 cilindra: ', len(vozila_4_cilindra))
print('Prosjecna emisija CO2 sa 4 cilindra: ', vozila_4_cilindra[['CO2 Emissions (g/km)']].mean())

vozila_6_cilindra = data[data['Cylinders'] == 6]
print('Broj vozila sa 6 cilindra: ', len(vozila_6_cilindra))
print('Prosjecna emisija CO2 sa 6 cilindra: ', vozila_6_cilindra[['CO2 Emissions (g/km)']].mean())

vozila_8_cilindra = data[data['Cylinders'] == 8]
print('Broj vozila s 8 cilindra: ', len(vozila_8_cilindra))
print('Prosjecna emisija CO2 sa 8 cilindra: ', vozila_8_cilindra[['CO2 Emissions (g/km)']].mean())

#f)
vozila_dizel = data[data['Fuel Type'] == 'D']
print('Prosjecna gradska potrosnja dizelasa: ', vozila_dizel[['Fuel Consumption City (L/100km)']].mean())
print('Medijan gradske potrosnje dizelasa: ', vozila_dizel[['Fuel Consumption City (L/100km)']].median())

vozila_benzin = data[data['Fuel Type'] == 'X']
print('Prosjecna gradska potrosnja benzinaca: ', vozila_benzin[['Fuel Consumption City (L/100km)']].mean())
print('Medijan gradske potrosnje benzinaca: ', vozila_benzin[['Fuel Consumption City (L/100km)']].median())

#g)
vozila_4_cilindra_dizel = vozila_4_cilindra[vozila_4_cilindra['Fuel Type'] == 'D']
vozilo_4_cilindra_dizel_potrosnja = vozila_4_cilindra_dizel.sort_values(by=['Fuel Consumption City (L/100km)'], ascending=False).head(1)
print('Vozilo s 4 cilindra koje koristi dizelski motor s najvecom gradskom potrosnjom goriva:\n ', vozilo_4_cilindra_dizel_potrosnja[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#h)
vozila_rucni_mjenjac = data[(data['Transmission'] == 'M5') | (data['Transmission'] == 'M6') | (data['Transmission'] == 'M7')]
print('Broj vozila s rucnim mjenjacem: ', len(vozila_rucni_mjenjac))

#i)
print('Korelacija numerickih velicina\n', data.corr(numeric_only=True))