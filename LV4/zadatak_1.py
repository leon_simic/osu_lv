from sklearn import datasets
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

data = pd.read_csv('data_C02_emission.csv')

# a)
input_variables = ['Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)',
                   'Engine Size (L)',
                   'Cylinders']

output_variable = ['CO2 Emissions (g/km)']
X = data[input_variables].to_numpy()
Y = data[output_variable].to_numpy()

X_train, X_test, Y_train, Y_test = train_test_split(
    X, Y, test_size=0.2, random_state=1)

# b)
# for i in range(len(input_variables)):
# train = plt.scatter(X_train[:, i], Y_train, c="blue")
# test = plt.scatter(X_test[:, i], Y_test, c="red", alpha=0.5)
# plt.xlabel(input_variables[i])
# plt.ylabel('CO2 Emissions (g/km)')
# plt.legend((train, test), ('Trening podaci', 'Testni podaci'))
# plt.show()

# c)
# plt.figure()
# plt.hist(X_train[:, 0])
# plt.title('Prije standardizacije')
# plt.show()

# print(X_train[:, 0])

scaler = StandardScaler()
X_train_n = scaler.fit_transform(X_train)

# plt.figure()
# plt.hist(X_train_n[:, 0])
# plt.title('Poslije standardizacije')
# plt.show()

X_test_n = scaler.transform(X_test)

# d)
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, Y_train)
print(linearModel.coef_)

# e)
Y_test_p = linearModel.predict(X_test_n)

plt.figure()
# for i in range(len(input_variables)):
#     stvarne = plt.scatter(X_test[:, i], Y_test, c="blue")
#     procjena = plt.scatter(X_test[:, i], Y_test_p, c="red", alpha=0.5)
#     plt.xlabel(input_variables[i])
#     plt.ylabel('CO2 Emissions (g/km)')
#     plt.legend((stvarne, procjena), ('Stvarne vrijednosti', 'Procjena modela'))
#     plt.show()

# f)
MAPE = mean_absolute_percentage_error(Y_test, Y_test_p)
print('MAPE: ', MAPE)

MAE = mean_absolute_error(Y_test, Y_test_p)
print('MAE: ', MAE)

r2 = r2_score(Y_test, Y_test_p)
print(r2)

# g)
# Rastu sve pogreške i model je manje točniji
# Ako maknemo 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)':
# MAPE:  0.043965626527762876
# MAE:  11.41553963196872
# 0.9146307345193577
