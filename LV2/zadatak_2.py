import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt("data.csv", delimiter=',', skip_header=1)

#a)
rows, cols = np.shape(data)
print("Mjerenja su izvrsena na " + str(rows) + " osoba.")   

#b)
height = data[:,1]
weight = data[:,2]

plt.scatter(height, weight)
plt.xlabel("Visina [cm]")
plt.ylabel("Masa [kg]")
plt.title("Odnos visine i mase")
#plt.show()

#c)
height50 = height[::50]
weight50 = weight[::50]

plt.scatter(height50, weight50)
plt.xlabel("Visina [cm]")
plt.ylabel("Masa [kg]")
plt.title("Odnos visine i mase svake 50. osobe")
#plt.show()

#d)
print("Minimalna vrijednost visine: " + str(np.min(height)))
print("Maksimalna vrijednost visine: " + str(np.max(height)))
print("Srednja vrijednost visine: " + str(np.mean(height)))
print()

#e)
menindex = (data[:,0] == 1)
menheight = []

for i in range(len(height)):
    if menindex[i]:
        menheight.append(height[i])

print("Minimalna vrijednost visine za muskarce: " + str(np.min(menheight)))
print("Maksimalna vrijednost visine za muskarce: " + str(np.max(menheight)))
print("Srednja vrijednost visine za muskarce: " + str(np.mean(menheight)))
print()

womenindex = (data[:,0] == 0)
womenheight = []

for i in range(len(height)):
    if womenindex[i]:
        womenheight.append(height[i])

print("Minimalna vrijednost visine za zene: " + str(np.min(womenheight)))
print("Maksimalna vrijednost visine za zene: " + str(np.max(womenheight)))
print("Srednja vrijednost visine za zene: " + str(np.mean(womenheight)))