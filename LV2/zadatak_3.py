import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
img = img[:,:,0].copy()

plt.figure()
plt.imshow(img, cmap="gray")
plt.title("original")
plt.show()

#a)
plt.figure()
plt.imshow(img, cmap="gray", alpha = 0.5)
plt.title("brighter")
plt.show()

#b)
width = len(img.T)
plt.figure()
plt.imshow(img[:, int(width/4):int(2*width/4)], cmap="gray")
plt.title("druga cetvrtina po sirini")
plt.show()

#c)
plt.figure()
plt.imshow(np.rot90(img, 3), cmap="gray")
plt.title("rotirano")
plt.show()

#d)
plt.figure()
plt.imshow(np.fliplr(img), cmap="gray")
plt.title("zrcalno")
plt.show()