import numpy as np
import matplotlib.pyplot as plt

white = np.ones((50, 50))
black = np.zeros((50, 50))

first = np.vstack((black, white))
second = np.vstack((white, black))
img = np.hstack((first, second))

plt.figure()
plt.imshow(img, cmap="gray")
plt.title("matrica")
plt.show()