import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV

# ucitaj podatke
data = pd.read_csv("Social_Network_Ads.csv")
print(data.info())

data.hist()
plt.show()

# dataframe u numpy
X = data[["Age","EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

# podijeli podatke u omjeru 80-20%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

# skaliraj ulazne velicine
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform((X_test))

KNN_model = KNeighborsClassifier()
KNN_model.fit(X_train_n, y_train)

param_grid = {'n_neighbors': np.arange(1, 101)}

KNN_gscv = GridSearchCV(KNN_model, param_grid, cv=5, scoring='accuracy', n_jobs=-1)
KNN_gscv.fit(X_train_n, y_train)
print('Best params: ', KNN_gscv.best_params_)