file = open("song.txt")
dictionary = {}

for line in file:
    line = line.rstrip()
    line = line.lower()
    words = line.split()

    for word in words:
        word = word.replace(",", "")
        if not dictionary.__contains__(word):
            dictionary[word] = 1
        elif dictionary.__contains__(word):
            dictionary[word] += 1
file.close

brojac = 0
for word in dictionary:
    if dictionary[word] == 1:
        print(word)
        brojac += 1

print("Ima ih " + str(brojac))
print(dictionary)
