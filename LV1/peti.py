file = open("SMSSpamCollection.txt")

hams = []
spams = []

for line in file:
    line = line.strip().replace("\t", "")

    if line.startswith("ham"):
        hams.append(line.replace("ham", ""))
    elif line.startswith("spam"):
        spams.append(line.replace("spam", ""))    

file.close

words = 0
for sms in hams:
    sms = sms.split()
    words += len(sms)
    
averageinham = words / len(hams)
print("Prosjecan broj rijeci u SMS koje su ham: " + str(averageinham))

words = 0
exclamations = 0

for sms in spams:
    if sms.endswith("!"):
        exclamations += 1

    sms = sms.split()
    words += len(sms)
    
averageinspam = words / len(spams)
print("Prosjecan broj rijeci u SMS koje su spam: " + str(averageinspam))
print("SMS spam poruka koje zavrsavaju usklicnikom ima: " + str(exclamations))